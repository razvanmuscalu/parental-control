package com.bskyb.internettv.parental_control_service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public enum ParentalControl {

    UNIVERSAL("U"),
    PARENTAL_GUIDANCE("PG"),
    OVER_12("12"),
    OVER_15("15"),
    OVER_18("18");

    private static final Map<String, ParentalControl> map = new HashMap<>();

    private final String name;

    ParentalControl(String name) {
        this.name = name;
    }

    static {
        for (ParentalControl parentalControl : ParentalControl.values()) {
            map.put(parentalControl.name, parentalControl);
        }
    }

    public static Optional<ParentalControl> value(String parentalControl) {
        return Optional.ofNullable(map.get(parentalControl));
    }

}
