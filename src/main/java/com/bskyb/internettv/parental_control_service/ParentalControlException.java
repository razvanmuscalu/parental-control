package com.bskyb.internettv.parental_control_service;

public class ParentalControlException extends RuntimeException {

    private final String message;

    public ParentalControlException(String message) {
        this.message = message;
    }
}
