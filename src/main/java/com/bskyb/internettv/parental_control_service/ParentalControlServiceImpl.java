package com.bskyb.internettv.parental_control_service;

import com.bskyb.internettv.thirdparty.MovieService;
import com.bskyb.internettv.thirdparty.TechnicalFailureException;

import java.util.function.BiFunction;

import static com.bskyb.internettv.parental_control_service.ParentalControl.value;

/*
 Requirements were vague regarding what the service should return when parental control level is outside the defined list.
 In this case, I assumed the service should return false. Alternatively, it could indicate the error to the calling client.
 */
public class ParentalControlServiceImpl implements ParentalControlService {

    private static final String CUSTOMER_PARENTAL_LEVEL_NOT_FOUND_EXCEPTION_MESSAGE = "cannot find customer parental control level";
    private static final String MOVIE_PARENTAL_LEVEL_NOT_FOUND_EXCEPTION_MESSAGE = "cannot find movie parental control level";

    private final MovieService movieService;

    private final BiFunction<String, String, Boolean> canWatch = (customerParentalControl, movieParentalControl) -> {
        int customerParentalControlOrdinal = value(customerParentalControl).orElseThrow(() -> new ParentalControlException(CUSTOMER_PARENTAL_LEVEL_NOT_FOUND_EXCEPTION_MESSAGE)).ordinal();
        int movieParentalControlOrdinal = value(movieParentalControl).orElseThrow(() -> new ParentalControlException(MOVIE_PARENTAL_LEVEL_NOT_FOUND_EXCEPTION_MESSAGE)).ordinal();

        return customerParentalControlOrdinal >= movieParentalControlOrdinal;
    };

    public ParentalControlServiceImpl(MovieService movieService) {
        this.movieService = movieService;
    }

    @Override
    public boolean canWatchMovie(String customerParentalControlLevel, String movieId) throws Exception {
        String movieParentalControlLevel = "";

        try {
            movieParentalControlLevel = movieService.getParentalControlLevel(movieId);
        } catch (TechnicalFailureException e) {
            return false;
        }

        try {
            return canWatch.apply(customerParentalControlLevel, movieParentalControlLevel);
        } catch (ParentalControlException e) {
            return false;
        }
    }

}