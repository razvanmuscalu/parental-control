package com.bskyb.internettv.parental_control_service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Optional;

import static com.bskyb.internettv.parental_control_service.ParentalControl.value;
import static java.util.Optional.empty;
import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class ParentalControlTest {

    @Test
    public void shouldReturnEmptyOptionalIfParentalControlIsUndefined() {
        Optional<ParentalControl> result = value("undefined");
        assertEquals(result, empty());
    }

    @Test
    public void shouldReturnEmptyOptionalIfParentalControlIsNull() {
        Optional<ParentalControl> result = value(null);
        assertEquals(result, empty());
    }

}