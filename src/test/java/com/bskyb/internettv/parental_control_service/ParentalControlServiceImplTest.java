package com.bskyb.internettv.parental_control_service;

import com.bskyb.internettv.thirdparty.MovieService;
import com.bskyb.internettv.thirdparty.TechnicalFailureException;
import com.bskyb.internettv.thirdparty.TitleNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;

@SuppressWarnings("unchecked")
@RunWith(MockitoJUnitRunner.class)
public class ParentalControlServiceImplTest {

    @Mock
    private MovieService movieService;

    private ParentalControlServiceImpl sut;

    @Before
    public void setUp() {
        sut = new ParentalControlServiceImpl(movieService);
    }

    @Test
    public void shouldReturnFalseByDefault() throws Exception {
        boolean result = sut.canWatchMovie("", "");

        assertThat("should return false by default", result, is(false));
    }

    @Test
    public void shouldReturnTrueWhenLevelsMatch() throws Exception {
        given(movieService.getParentalControlLevel("Avengers")).willReturn("PG");

        boolean result = sut.canWatchMovie("PG", "Avengers");

        assertThat("should return true when levels match", result, is(true));
    }

    @Test
    public void shouldReturnFalseWhenMovieServiceThrowsTechnicalFailureException() throws Exception {
        given(movieService.getParentalControlLevel("Avengers")).willThrow(TechnicalFailureException.class);

        boolean result = sut.canWatchMovie("PG", "Avengers");

        assertThat("should return false when movie service throws TechnicalFailureException", result, is(false));
    }

    @Test(expected = TitleNotFoundException.class)
    public void shouldThrowTitleNotFoundExceptionWhenMovieServiceThrowsTitleNotFoundException() throws Exception {
        given(movieService.getParentalControlLevel("Avengers")).willThrow(TitleNotFoundException.class);

        sut.canWatchMovie("PG", "Avengers");
    }

    @Test
    public void shouldReturnTrueWhenCustomerParentalControlLevelHigherThanMovieParentalControlLevel() throws Exception {
        given(movieService.getParentalControlLevel("Avengers")).willReturn("PG");

        boolean result = sut.canWatchMovie("12", "Avengers");

        assertThat("should return true when customer parental control level higher than movie parental control level", result, is(true));
    }

    @Test
    public void shouldReturnFalseWhenCustomerParentalControlLevelLowerThanMovieParentalControlLevel() throws Exception {
        given(movieService.getParentalControlLevel("Avengers")).willReturn("12");

        boolean result = sut.canWatchMovie("PG", "Avengers");

        assertThat("should return false when customer parental control level lower than movie parental control level", result, is(false));
    }

    @Test
    public void shouldReturnFalseWhenMovieServiceReturnsUndefinedParentalControlLevel() throws Exception {
        given(movieService.getParentalControlLevel("Avengers")).willReturn("undefined");

        boolean result = sut.canWatchMovie("PG", "Avengers");

        assertThat("should return false movie service returns undefined parental control level", result, is(false));
    }

    @Test
    public void shouldReturnFalseWhenCustomerParentalControlLevelIsUndefined() throws Exception {
        given(movieService.getParentalControlLevel("Avengers")).willReturn("PG");

        boolean result = sut.canWatchMovie("undefined", "Avengers");

        assertThat("should return false customer parental control level is undefined", result, is(false));
    }

    @Test
    public void shouldReturnFalseWhenMovieServiceReturnsNull() throws Exception {
        given(movieService.getParentalControlLevel("Avengers")).willReturn(null);

        boolean result = sut.canWatchMovie("PG", "Avengers");

        assertThat("should return false movie service returns null", result, is(false));
    }

    @Test
    public void shouldReturnFalseWhenCustomerParentalControlLevelIsNull() throws Exception {
        given(movieService.getParentalControlLevel("Avengers")).willReturn("PG");

        boolean result = sut.canWatchMovie(null, "Avengers");

        assertThat("should return false customer parental control level is null", result, is(false));
    }

}