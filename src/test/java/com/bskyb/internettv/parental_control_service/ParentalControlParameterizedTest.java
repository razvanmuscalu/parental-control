package com.bskyb.internettv.parental_control_service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;

import static com.bskyb.internettv.parental_control_service.ParentalControl.value;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(Parameterized.class)
public class ParentalControlParameterizedTest {

    @Parameters(name = "ParentalControl.{0} should have ordinal: {1}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"U", 0},
                {"PG", 1},
                {"12", 2},
                {"15", 3},
                {"18", 4}
        });
    }

    private final String parentalControlValue;
    private final int expectedParentalControlOrdinal;

    public ParentalControlParameterizedTest(String parentalControlValue, int expectedParentalControlOrdinal) {
        this.parentalControlValue = parentalControlValue;
        this.expectedParentalControlOrdinal = expectedParentalControlOrdinal;
    }

    @Test
    public void shouldReturnCorrectOrdinals() {
        int result = value(parentalControlValue).get().ordinal();
        assertThat("should return correct parental control ordinal", result, is(expectedParentalControlOrdinal));
    }

}